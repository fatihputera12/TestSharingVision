<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posts;

class post extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $publish  = Posts::where('status','publish')->get();
        $draft  = Posts::where('status','draft')->get();
        $trash  = Posts::where('status','trash')->get();
        return view('posts/index',compact('publish','draft','trash'));
    }

    public function preview()
    {
        $data  = Posts::where('status','publish')->paginate(6);
        return view('posts/preview',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $posts = new Posts();
        $posts->title = $request->title;
        $posts->content = $request->content;
        $posts->category = $request->category;
        $posts->status = $request->status;
        $posts->save();
      
        return redirect('posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Posts::find($id);
        return view('posts/edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $posts = Posts::find($id);
        $posts->title = $request->title;
        $posts->content = $request->content;
        $posts->category = $request->category;
        $posts->status = $request->status;
        $posts->save();
      
        return redirect('posts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $posts = Posts::find($id);
        $posts->status = "trash";
        $posts->save();
      
        return redirect('posts');
    }

}
