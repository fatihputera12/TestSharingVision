@extends('layout/layout')
@section('title','Preview')
@section('content')

    <div class="row">
        <?php $no = 1; ?>
        @foreach($data as $row)
            <?php $no++; ?>
            @if($no == 5)
            <div class="col-md-12 d-flex justify-content-center"><br></div>
            <div class="col-md-4 d-flex justify-content-center">
                <div class="card" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title">{{$row->title}}</h5>
                        <h6 class="card-subtitle mb-2 text-muted">{{$row->category}}</h6>
                        <p class="card-text">{{$row->content}}</p>
                    </div>
                </div>
            </div>
            @else
            <div class="col-md-4 d-flex justify-content-center">
                <div class="card" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title">{{$row->title}}</h5>
                        <h6 class="card-subtitle mb-2 text-muted">{{$row->category}}</h6>
                        <p class="card-text">{{$row->content}}</p>
                    </div>
                </div>
            </div>
            @endif
        @endforeach
        <div class="col-md-12 d-flex justify-content-center"><br><br></div>
        <div class="col-md-3 d-flex justify-content-center">
            Halaman : {{ $data->currentPage() }} 
        </div>
        <div class="col-md-3 d-flex justify-content-center">
            Jumlah Data : {{ $data->total() }} 
        </div>
        <div class="col-md-3 d-flex justify-content-center">
            Data Per Halaman : {{ $data->perPage() }} 
        </div>
        <div class="col-md-3 d-flex justify-content-center">
            {{ $data->links() }}
        </div>
    </div>


@endsection