@extends('layout/layout')
@section('title','Posts')
@section('content')

    <div class="row">
        <div class="col-md-12 d-flex justify-content-center">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Published ({{count($publish)}})</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Drafts ({{count($draft)}})</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Trashed ({{count($trash)}})</button>
                </li>
            </ul>
        </div>
        <div class="col-md-12 d-flex justify-content-center"><br></div>
        <div class="col-md-12 d-flex justify-content-center">
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab" tabindex="0">
                    <table class="table myTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Title</th>
                                <th>category</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?>
                            @foreach($publish as $row)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{$row->title}}</td>
                                <td>{{$row->category}}</td>
                                <td>
                                    <a href="{{route('posts.edit',$row->id)}}" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                                    <a href="{{url('posts/delete/'.$row->id)}}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-12 d-flex justify-content-center">
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-home-tab" tabindex="0">
                    <table class="table myTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Title</th>
                                <th>category</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?>
                            @foreach($draft as $row)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{$row->title}}</td>
                                <td>{{$row->category}}</td>
                                <td>
                                    <a href="{{route('posts.edit',$row->id)}}" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                                    <a href="{{url('posts/delete/'.$row->id)}}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-12 d-flex justify-content-center">
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-home-tab" tabindex="0">
                    <table class="table myTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Title</th>
                                <th>category</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?>
                            @foreach($trash as $row)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{$row->title}}</td>
                                <td>{{$row->category}}</td>
                                <td>
                                    <a href="{{route('posts.edit',$row->id)}}" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    

@endsection