@extends('layout/layout')
@section('title','Edit Posts')
@section('content')

    <div class="row">
        <div class="col-md-12 d-flex justify-content-center">
            <div class="card">
                <div class="card-header">
                    Edit Post
                </div>
                <div class="card-body">
                    <form method="post" action="{{route('posts.update',$data->id)}}">
                    @METHOD('PUT')
                    @csrf
                        <div class="mb-3">
                            <label for="title" class="form-label">Title</label>
                            <input type="text" class="form-control" id="title" name="title" required value="{{$data->title}}">
                        </div>
                        <div class="mb-3">
                            <label for="content" class="form-label">Content</label>
                            <textarea name="content" id="content" cols="50" rows="10" class="form-control" required>{{$data->content}}</textarea>
                        </div>
                        <div class="mb-3">
                            <label for="category" class="form-label">Category</label>
                            <input type="text" class="form-control" id="category" name="category" required value="{{$data->category}}">
                        </div>
                        <div class="mb-3">
                            <label for="status" class="form-label">Status</label> <br>
                            <input type="radio" id="status" name="status" required value="publish" <?php if($data->status == 'publish'){ echo "checked"; } ?>> Publish
                            <input type="radio" id="status" name="status" value="draft" <?php if($data->status == 'draft'){ echo "checked"; } ?>> Draft
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection