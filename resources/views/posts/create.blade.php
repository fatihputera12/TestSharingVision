@extends('layout/layout')
@section('title','Create Posts')
@section('content')

    <div class="row">
        <div class="col-md-12 d-flex justify-content-center">
            <div class="card">
                <div class="card-header">
                    Create Post
                </div>
                <div class="card-body">
                    <form method="post" action="{{route('posts.store')}}">
                    @csrf
                        <div class="mb-3">
                            <label for="title" class="form-label">Title</label>
                            <input type="text" class="form-control" id="title" name="title" required>
                        </div>
                        <div class="mb-3">
                            <label for="content" class="form-label">Content</label>
                            <textarea name="content" id="content" cols="50" rows="10" class="form-control" required></textarea>
                        </div>
                        <div class="mb-3">
                            <label for="category" class="form-label">Category</label>
                            <input type="text" class="form-control" id="category" name="category" required>
                        </div>
                        <div class="mb-3">
                            <label for="status" class="form-label">Status</label> <br>
                            <input type="radio" id="status" name="status" required value="publish"> Publish
                            <input type="radio" id="status" name="status" value="draft"> Draft
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection